const textArea=document.getElementById("message");
const ul=document.getElementById("chat_list");

const sendMessage = function () {
    if (textArea.value) {
        const li=document.createElement("li");
        li.className = "person2";

        const img = document.createElement("img");
        img.src="https://randomuser.me/api/portraits/lego/2.jpg";
        img.alt="avatar2";
        img.className="avatar";

        const span=document.createElement("span");
        span.innerText=textArea.value;

        li.appendChild(img);
        li.appendChild(span);

        ul.appendChild(li);
        textArea.value="";
    }
};
